#!/bin/bash

#script to setup a  new mahcine for python development
# This will install softwares required. Please go through the script, to check and comment out 
# packages/softwares which you don't want to install.
# run this script from your home dir, ex : /home/arun/


#make applications directory
mkdir pyapps

#make dir to install/create virtual enviornments
mkdir pyenvs

sudo apt-get install build-essential pkg-config  graphviz libgraphviz-dev python-dev python-distribute

#databases
sudo apt-get install postgresql libpq-dev

#webservers
sudo apt-get install apache2 libapache2-mod-wsgi

#Version control system
sudo apt-get install git


#python package manager
easy_install pip

#python virtualenviornment
pip install virtualenv


#imaging libraries
sudo apt-get install libjpeg8 libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev


#rabbitmq
#sudo apt-get install rabbitmq-server