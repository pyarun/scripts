#!/bin/bash

apt-get install git

#add firefox repo
add-apt-repository ppa:ubuntu-mozilla-security/ppa

#add skype repo
dpkg --add-architecture i386
add-apt-repository "deb http://archive.canonical.com/ $(lsb_release -sc) partner"



mkdir -p workspace

mkdir -p pyenvs

echo "Create a ssh key for your account"
#ssh-keygen

apt-get update

#install required softwares
apt-get install build-essential pkg-config  graphviz libgraphviz-dev python-dev python-distribute postgresql libpq-dev \
 mysql-server nginx apache2 libapache2-mod-wsgi mongodb libjpeg8 libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev \
 openjdk-7-jre skype firefox chromium-browser filezilla vim 
 
#install python pacakges
easy_install pip
#pip cache setting
echo "[global]" >> $HOME/.pip/pip.conf
echo "download-cache=$HOME/.pip/cache" >> $HOME/.pip/pip.conf
#pacakages
pip install virtualenv virtualenvwrapper pylint fabric ipython ipdb
#vitualenvwrapper setting
echo "export WORKON_HOME=$HOME/pyenvs" >> $HOME/.bashrc
echo "export PROJECT_HOME=$HOME/workspace" >> $HOME/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> $HOME/.bashrc











