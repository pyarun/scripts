
/** function to parse the query string to js object */
function parseQuery(qstr)
{
	var query = {};
	var a = qstr.split('&');
	for (var i in a)
	{
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1]);
	}
	
	return query;
}